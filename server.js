var express = require("express");
var app = express();

app.get('/', function(req,res) {
	req.send("I am alive!");
});

app.listen('3000', function() {
	console.log("Server running on localhost:3000");
});
